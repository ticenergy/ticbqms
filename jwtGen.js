const jwt = require('jsonwebtoken');

// Clé secrète JWT
const JWT_SECRET = 'MySuperSecretJWTKey123!';

// Données de l'utilisateur pour générer le token
const userData = {
  id: 1,
  username: 'johndoe',
  role: 'admin'
};

// Générer le token JWT
const token = jwt.sign(userData, JWT_SECRET);

console.log(token);

