// controllers.js

// Importer les modules et les dépendances nécessaires
const User = require('./models/user'); // Importer le modèle User
const Queue = require('./models/queue'); // Importer le modèle Queue
const Ticket = require('./models/ticket'); // Importer le modèle Ticket

// Contrôleur pour la création d'un utilisateur
async function createUser(req, res) {
  try {
    // Récupérer les données du nouvel utilisateur depuis la requête
    const userData = req.body;

    // Créer une instance de User avec les données fournies
    const user = new User(userData);

    // Sauvegarder l'utilisateur dans la base de données
    await user.save();

    // Répondre avec succès et renvoyer les détails de l'utilisateur créé
    res.status(201).json(user);
  } catch (error) {
    // En cas d'erreur, répondre avec une erreur et le message d'erreur correspondant
    res.status(500).json({ error: error.message });
  }
}

// Contrôleur pour la création d'une file d'attente
async function createQueue(req, res) {
  try {
    // Récupérer les données de la nouvelle file d'attente depuis la requête
    const queueData = req.body;

    // Créer une instance de Queue avec les données fournies
    const queue = new Queue(queueData);

    // Sauvegarder la file d'attente dans la base de données
    await queue.save();

    // Répondre avec succès et renvoyer les détails de la file d'attente créée
    res.status(201).json(queue);
  } catch (error) {
    // En cas d'erreur, répondre avec une erreur et le message d'erreur correspondant
    res.status(500).json({ error: error.message });
  }
}

// Contrôleur pour la création d'un ticket dans une file d'attente
async function createTicket(req, res) {
  try {
    // Récupérer les données du nouveau ticket depuis la requête
    const ticketData = req.body;

    // Créer une instance de Ticket avec les données fournies
    const ticket = new Ticket(ticketData);

    // Sauvegarder le ticket dans la base de données
    await ticket.save();

    // Répondre avec succès et renvoyer les détails du ticket créé
    res.status(201).json(ticket);
  } catch (error) {
    // En cas d'erreur, répondre avec une erreur et le message d'erreur correspondant
    res.status(500).json({ error: error.message });
  }
}

// Exporter les contrôleurs pour les rendre accessibles depuis d'autres modules
module.exports = {
  createUser,
  createQueue,
  createTicket
};

