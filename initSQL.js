const fs = require('fs');
const path = require('path');
// const pool = require('./dbConfig');
const pool = require('./dbConfig');

// Fonction pour exécuter une requête SQL
async function executeQuery(query) {
  try {
    await pool.query(query);
    console.log('Query executed successfully.');
  } catch (error) {
    console.error('Error executing query:', error);
  }
}

// Fonction pour lire et exécuter le contenu d'un fichier SQL
async function executeSQLFile(filePath) {
  try {
    const sql = fs.readFileSync(filePath, 'utf8');
    await executeQuery(sql);
  } catch (error) {
    console.error('Error executing SQL file:', error);
  }
}

// Chemin vers les fichiers SQL
const appSQLFilePath = path.join(__dirname, 'app.sql');
const initSQLFilePath = path.join(__dirname, 'init.sql');

// Initialisation de la base de données
async function initializeDatabase() {
  console.log('Initializing database...');

  // Exécution du fichier app.sql
  console.log('Executing app.sql');
  await executeSQLFile(appSQLFilePath);

  // Exécution du fichier init.sql
  console.log('Executing init.sql');
  await executeSQLFile(initSQLFilePath);

  console.log('Database initialization complete.');
}

// Appel de la fonction d'initialisation de la base de données
initializeDatabase();

