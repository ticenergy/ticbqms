const express = require('express');
const jwt = require('jsonwebtoken');
const User = require('./models/user');
const Queue = require('./models/queue');
const Ticket = require('./models/ticket');
const {
  createUser,
  createQueue,
  createTicket
} = require('./controllers');

const app = express();

// Middleware pour vérifier l'authentification avec JWT
function authenticateToken(req, res, next) {
  const authHeader = req.headers['authorization'];
  const token = authHeader && authHeader.split(' ')[1];

  if (token == null) {
    return res.sendStatus(401);
  }

  jwt.verify(token, process.env.JWT_SECRET || 'MySuperSecretJWTKey123!' , (err, user) => {
    
    if (err) {
      return res.sendStatus(403);
    }
    req.user = user;
    next();
  });
}

// Middleware pour parser les données JSON
app.use(express.json());

// Route pour créer un nouvel utilisateur
app.post('/users', createUser);

// Route pour créer une nouvelle file d'attente (authentification requise)
app.post('/queues', authenticateToken, createQueue);

// Route pour créer un nouveau ticket dans une file d'attente (authentification requise)
app.post('/tickets', authenticateToken, createTicket);

// Route de test pour vérifier l'authentification avec JWT
app.get('/test', authenticateToken, (req, res) => {
  res.json(req.user);
});

// Port d'écoute du serveur
const port = process.env.PORT || 3000;

// Démarrer le serveur
app.listen(port, () => {
  console.log(`Server listening on port ${port}`);
});

