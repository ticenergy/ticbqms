TOKEN=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwidXNlcm5hbWUiOiJqb2huZG9lIiwicm9sZSI6ImFkbWluIiwiaWF0IjoxNjg5MjA0Mzk4fQ.jjJmn1doyV0ED0vs90aXOWGCLVNQTKar6gp-_yA7yOU


# Remplacez <TOKEN> par un véritable token JWT valide
curl -H "Authorization: Bearer $TOKEN" http://localhost:3000/test





## Créer un nouvel utilisateur :
#
#curl -X POST -H "Content-Type: application/json" -d '{
#	"first_name": "John",
#	"last_name": "Doe",
#	"email": "john.doe@example.com",
#	"phone_number": "1234567890",
#	"user_type": "client",
#	"username": "johndoe",
#	"password": "password"
#}' http://localhost:3000/users




## Créer une nouvelle file d'attente (avec authentification JWT) :
#
# Remplacez <TOKEN> par un véritable token JWT valide
#curl -X POST -H "Content-Type: application/json" -H "Authorization: Bearer <TOKEN>" -d '{
# curl -X POST -H "Content-Type: application/json" -H "Authorization: Bearer $TOKEN " -d '{
#   "name": "Queue 1",
#   "description": "First queue"
# }' http://localhost:3000/queues


## Remplacez <TOKEN> par un véritable token JWT valide
#curl -X POST -H "Content-Type: application/json" -H "Authorization: Bearer $TOKEN" -d '{
#  "queue_id": 1,
#  "user_id": 1,
#  "ticket_number": 1
#}' http://localhost:3000/tickets


