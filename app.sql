-- Table pour les utilisateurs (clients)
CREATE TABLE users (
  id SERIAL PRIMARY KEY,
  first_name VARCHAR(50) NOT NULL,
  last_name VARCHAR(50) NOT NULL,
  email VARCHAR(100) UNIQUE NOT NULL,
  phone_number VARCHAR(20) NOT NULL,
  preferences VARCHAR(255),
  user_type VARCHAR(10) NOT NULL DEFAULT 'guest' CHECK (user_type IN ('guest', 'client', 'provider', 'agent', 'admin')),
  username VARCHAR(50),
  password_hash VARCHAR(255),
  language VARCHAR(10) DEFAULT 'en' NOT NULL, -- Ajout de la colonne de langue
  created_at TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP
);

-- Table pour les files d'attente
CREATE TABLE queues (
  id SERIAL PRIMARY KEY,
  name VARCHAR(100) NOT NULL,
  description VARCHAR(255),
  created_at TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP
);

-- Table pour les tickets de file d'attente
CREATE TABLE tickets (
  id SERIAL PRIMARY KEY,
  queue_id INT NOT NULL,
  user_id INT NOT NULL,
  ticket_number INT NOT NULL,
  priority INT,
  estimated_wait_time INT,
  called BOOLEAN DEFAULT FALSE,
  redirected_to_queue_id INT,
  created_at TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP,
  FOREIGN KEY (queue_id) REFERENCES queues(id),
  FOREIGN KEY (user_id) REFERENCES users(id),
  FOREIGN KEY (redirected_to_queue_id) REFERENCES queues(id)
);

-- Table pour les statistiques des files d'attente
CREATE TABLE queue_statistics (
  id SERIAL PRIMARY KEY,
  queue_id INT NOT NULL,
  average_wait_time INT,
  total_tickets INT,
  open_tickets INT,
  closed_tickets INT,
  created_at TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP,
  FOREIGN KEY (queue_id) REFERENCES queues(id)
);

-- Table for internationalization
CREATE TABLE i18n (
  id SERIAL PRIMARY KEY,
  key VARCHAR(100) NOT NULL,
  lang VARCHAR(10) NOT NULL,
  value TEXT NOT NULL,
  created_at TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP
);

