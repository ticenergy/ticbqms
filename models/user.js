const pool = require('../dbConfig');

// Modèle User
class User {
  constructor(data) {
    this.data = data;
  }

  async save() {
    const query = {
      text: `
        INSERT INTO users (
          first_name,
          last_name,
          email,
          phone_number,
          preferences,
          user_type,
          username,
          password_hash,
          language
        )
        VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9)
        RETURNING *
      `,
      values: [
        this.data.first_name,
        this.data.last_name,
        this.data.email,
        this.data.phone_number,
        this.data.preferences,
        this.data.user_type,
        this.data.username,
        this.data.password_hash,
        this.data.language || 'en', // Par défaut, la langue est "en"
      ],
    };

    try {
      const result = await pool.query(query);
      this.data = result.rows[0];
      return this.data;
    } catch (error) {
      throw new Error(`Error saving user: ${error}`);
    }
  }
}

module.exports = User;

