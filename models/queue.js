const pool = require('../dbConfig');

// Modèle Queue
class Queue {
  constructor(data) {
    this.data = data;
  }

  async save() {
    const query = {
      text: `
        INSERT INTO queues (
          name,
          description
        )
        VALUES ($1, $2)
        RETURNING *
      `,
      values: [
        this.data.name,
        this.data.description,
      ],
    };

    try {
      const result = await pool.query(query);
      this.data = result.rows[0];
      return this.data;
    } catch (error) {
      throw new Error(`Error saving queue: ${error}`);
    }
  }
}

module.exports = Queue;

