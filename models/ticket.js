const pool = require('../dbConfig');

// Modèle Ticket
class Ticket {
  constructor(data) {
    this.data = data;
  }

  async save() {
    const query = {
      text: `
        INSERT INTO tickets (
          queue_id,
          user_id,
          ticket_number,
          priority,
          estimated_wait_time,
          called,
          redirected_to_queue_id
        )
        VALUES ($1, $2, $3, $4, $5, $6, $7)
        RETURNING *
      `,
      values: [
        this.data.queue_id,
        this.data.user_id,
        this.data.ticket_number,
        this.data.priority || null,
        this.data.estimated_wait_time || null,
        this.data.called || false,
        this.data.redirected_to_queue_id || null,
      ],
    };

    try {
      const result = await pool.query(query);
      this.data = result.rows[0];
      return this.data;
    } catch (error) {
      throw new Error(`Error saving ticket: ${error}`);
    }
  }
}

module.exports = Ticket;

