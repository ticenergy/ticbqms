const pool = require('./dbConfig');

// Test the database connection
pool
  .connect()
  .then(() => {
    console.log('Connected to the database');
    // Execute a test query
    pool.query('SELECT NOW()', (err, res) => {
      if (err) {
        console.error('Error executing query:', err);
      } else {
        console.log('Query result:', res.rows[0]);
      }
      // End the database connection
      pool.end();
    });
  })
  .catch((err) => {
    console.error('Error connecting to the database:', err);
  });

